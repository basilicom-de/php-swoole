# Customized php 7.2-cli Docker image with Swoole PHP extension

## Notes

Sample invocation using the blink swoole framework:

    docker run -p 8080:7788 --name php-swoole-blink \
        -v "$PWD":/php basilicom/php-swoole \
        /php/blink server:serve --live-reload


